# INDITEX API REST
This application uses:
1. Maven build tool
2. Spring Framework + Java 17
3. Unit Test with MockMvc
4. Swagger documentation
5. H2 Database
6. Spring Actuator
7. Docker

# How to run it?

- execute: git clone https://gitlab.com/andresRolo/inditex.git in 'folder'

- move to 'folder' and execute: mvn clean install

- execute: docker build -t prices:latest .

- execute: docker run -p 8080:8080 -t prices

# How to verify it?

Access to Actuator http://localhost:8080/actuator/health URL

Access the documentation in http://localhost:8080/swagger-ui.html URL

### Request Example

curl -X GET "http://localhost:8080/prices?application_date=2020-08-02T11%3A00%3A00.000Z&brand_id=1&product_id=35455" -H "accept: */*"

### Response Example

{
"brandId": 1,
"productId": 35455,
"priceList": 4,
"startDate": "2020-06-15T16:00:00",
"endDate": "2020-12-31T23:59:59",
"price": 38.95
}

# Could we improve it?
Definitly yes! We could add different tests (mutation, ITs, more UTs, etc), use OpenAPI, cache and more. We could discuss it