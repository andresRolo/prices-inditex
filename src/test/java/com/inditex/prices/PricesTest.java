package com.inditex.prices;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeTypeUtils;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class PricesTest {

    public static final String GET_PRICES_ENDPOINT = "/prices";
    public static final String BRAND_ID_PARAMETER = "brand_id";
    public static final String PRODUCT_ID_PARAMETER = "product_id";
    public static final String APPLICATION_DATE_PARAMETER = "application_date";

    @Autowired
    private MockMvc mockMvc;

    //Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
    @Test
    void givenTest1_thenStatus200() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "1")
                .param(PRODUCT_ID_PARAMETER, "35455")
                .param(APPLICATION_DATE_PARAMETER, "2020-06-14T10:00:00")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            //should return data of first record of data.sql file
            .andExpect(jsonPath("$.brandId", is(1)))
            .andExpect(jsonPath("$.productId", is(35455)))
            .andExpect(jsonPath("$.priceList", is(1)))
            .andExpect(jsonPath("$.startDate", is("2020-06-14T00:00:00")))
            .andExpect(jsonPath("$.endDate", is("2020-12-31T23:59:59")))
            .andExpect(jsonPath("$.price", is(35.50)));
    }

    //Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
    @Test
    void givenTest2_thenStatus200() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "1")
                .param(PRODUCT_ID_PARAMETER, "35455")
                .param(APPLICATION_DATE_PARAMETER, "2020-06-14T16:00:00")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            //should return data of second record of data.sql file
            .andExpect(jsonPath("$.brandId", is(1)))
            .andExpect(jsonPath("$.productId", is(35455)))
            .andExpect(jsonPath("$.priceList", is(2)))
            .andExpect(jsonPath("$.startDate", is("2020-06-14T15:00:00")))
            .andExpect(jsonPath("$.endDate", is("2020-06-14T18:30:00")))
            .andExpect(jsonPath("$.price", is(25.45)));
    }

    //Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)
    @Test
    void givenTest3_thenStatus200() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "1")
                .param(PRODUCT_ID_PARAMETER, "35455")
                .param(APPLICATION_DATE_PARAMETER, "2020-06-14T21:00:00")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            //should return data of first record of data.sql file
            .andExpect(jsonPath("$.brandId", is(1)))
            .andExpect(jsonPath("$.productId", is(35455)))
            .andExpect(jsonPath("$.priceList", is(1)))
            .andExpect(jsonPath("$.startDate", is("2020-06-14T00:00:00")))
            .andExpect(jsonPath("$.endDate", is("2020-12-31T23:59:59")))
            .andExpect(jsonPath("$.price", is(35.50)));
    }

    //Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)
    @Test
    void givenTest4_thenStatus200() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "1")
                .param(PRODUCT_ID_PARAMETER, "35455")
                .param(APPLICATION_DATE_PARAMETER, "2020-06-15T10:00:00")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            //should return data of third record of data.sql file
            .andExpect(jsonPath("$.brandId", is(1)))
            .andExpect(jsonPath("$.productId", is(35455)))
            .andExpect(jsonPath("$.priceList", is(3)))
            .andExpect(jsonPath("$.startDate", is("2020-06-15T00:00:00")))
            .andExpect(jsonPath("$.endDate", is("2020-06-15T11:00:00")))
            .andExpect(jsonPath("$.price", is(30.50)));
    }

    //Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)
    @Test
    void givenTest5_thenStatus200() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "1")
                .param(PRODUCT_ID_PARAMETER, "35455")
                .param(APPLICATION_DATE_PARAMETER, "2020-06-16T21:00:00")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            //should return data of fourth record of data.sql file
            .andExpect(jsonPath("$.brandId", is(1)))
            .andExpect(jsonPath("$.productId", is(35455)))
            .andExpect(jsonPath("$.priceList", is(4)))
            .andExpect(jsonPath("$.startDate", is("2020-06-15T16:00:00")))
            .andExpect(jsonPath("$.endDate", is("2020-12-31T23:59:59")))
            .andExpect(jsonPath("$.price", is(38.95)));
    }

    @Test
    void givenWrongRequest_thenStatus400() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "WRONG")
                .param(PRODUCT_ID_PARAMETER, "WRONG")
                .param(APPLICATION_DATE_PARAMETER, "WRONG")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(status().isBadRequest());
    }

    @Test
    void givenValidRequestButWithoutPriceFound_thenStatus404() throws Exception {

        mockMvc.perform(
            get(GET_PRICES_ENDPOINT)
                .contentType(APPLICATION_JSON)
                .param(BRAND_ID_PARAMETER, "1")
                .param(PRODUCT_ID_PARAMETER, "35455")
                .param(APPLICATION_DATE_PARAMETER, "2030-06-16T21:00:00")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

}
