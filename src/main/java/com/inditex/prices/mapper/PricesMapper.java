package com.inditex.prices.mapper;

import com.inditex.prices.dto.Price;
import com.inditex.prices.entity.PriceEntity;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PricesMapper {
  Price asPrice(PriceEntity priceEntity);
}
