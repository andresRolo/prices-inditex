package com.inditex.prices.service;

import com.inditex.prices.dto.Price;

public interface PricesService {

    Price getPrices(Price price);
}
