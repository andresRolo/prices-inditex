package com.inditex.prices.service.impl;

import java.util.List;

import com.inditex.prices.dto.Price;
import com.inditex.prices.entity.PriceEntity;
import com.inditex.prices.exceptions.NoPriceFoundException;
import com.inditex.prices.mapper.PricesMapper;
import com.inditex.prices.repository.PricesRepository;
import com.inditex.prices.service.PricesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PricesServiceImpl implements PricesService {

    private final PricesRepository pricesRepository;
    private final PricesMapper pricesMapper;

    @Autowired
    public PricesServiceImpl(final PricesRepository pricesRepository, final PricesMapper pricesMapper) {
        this.pricesRepository = pricesRepository;
        this.pricesMapper = pricesMapper;
    }

    @Override
    public Price getPrices(final Price price) {
        return findPrice(price)
                .stream()
                .reduce((a, b) -> a.getPriority() > b.getPriority() ? a : b)
                .map(this.pricesMapper::asPrice)
                .orElseThrow(NoPriceFoundException::new);
    }

    private List<PriceEntity> findPrice(Price price) {
        return this.pricesRepository.findAllByBrandIdAndProductIdAndEndDateGreaterThanEqualAndStartDateLessThanEqual(
            price.getBrandId(), price.getProductId(), price.getApplicationDate(),
            price.getApplicationDate());
    }

}
