package com.inditex.prices.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    public static final String NO_PRICE_FOUND_MESSAGE = "No price found for requested info";

    @ExceptionHandler(value = NoPriceFoundException.class)
    public ResponseEntity<Object> handlerNoPriceFoundException(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, NO_PRICE_FOUND_MESSAGE,
            new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}
