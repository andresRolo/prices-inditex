package com.inditex.prices.exceptions;

public class NoPriceFoundException extends RuntimeException {

    public NoPriceFoundException() {
        super();
    }

}
