package com.inditex.prices.controller;

import com.inditex.prices.dto.Price;
import com.inditex.prices.dto.PriceResponse;
import com.inditex.prices.mapper.PricesRestMapper;
import com.inditex.prices.service.PricesService;
import com.inditex.prices.util.Constants;
import com.sun.istack.NotNull;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@ResponseBody
@RequestMapping(Constants.pricesPath)
@Api(value = Constants.pricesValue,
        tags = {Constants.version})
public class PricesController {

    private final PricesService pricesService;
    
    private final PricesRestMapper pricesRestMapper;

    @Autowired
    public PricesController(PricesService pricesService, PricesRestMapper pricesRestMapper) {
        this.pricesService = pricesService;
        this.pricesRestMapper = pricesRestMapper;
    }

    @GetMapping()
    @ApiOperation(value = Constants.getPricesValue)
    public ResponseEntity<PriceResponse> getPrices(@RequestParam(name = "brand_id") @NotNull Integer brandId,
            @RequestParam(name = "product_id") @NotNull Integer productId,
            @RequestParam(name = "application_date") @NotNull
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime applicationDate) {
        return ResponseEntity.ok(this.pricesRestMapper.asPriceResponse(this.pricesService.getPrices(Price.builder()
                .brandId(brandId)
                .productId(productId)
                .applicationDate(applicationDate)
                .build())));
    }

}
