package com.inditex.prices.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceRequest {

    private Integer brandId;
    private Integer productId;
    private LocalDateTime applicationDate;
}
