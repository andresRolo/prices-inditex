package com.inditex.prices.repository;

import com.inditex.prices.entity.PriceEntity;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;


public interface PricesRepository extends CrudRepository<PriceEntity, Integer> {

	/**
	 * @param brandId          where brandId = 1st parameter
	 * @param productId        and productId = 2nd parameter
	 * @param applicationDate  and endDate is greater than 3er parameter
	 * @param applicationDate2 and startDate is less than 4th parameter
	 * @return List<PriceEntity>
	 */
	List<PriceEntity> findAllByBrandIdAndProductIdAndEndDateGreaterThanEqualAndStartDateLessThanEqual(int brandId,
                                                                                int productId,
                                                                                LocalDateTime applicationDate,
                                                                                LocalDateTime applicationDate2);
}
