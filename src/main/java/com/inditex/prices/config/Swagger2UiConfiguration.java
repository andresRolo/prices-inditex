package com.inditex.prices.config;

import com.inditex.prices.util.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class Swagger2UiConfiguration {
    private static final String ALL_CHILD = ".*";

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title(Constants.tittleAPI)
                .version(Constants.version)
                .build();
    }

    @Bean
    public Docket priceApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(Constants.priceAPIGroup)
                .apiInfo(metadata())
                .select()
                .paths(regex(Constants.pricesPath + ALL_CHILD)).build();

    }
    
    

}