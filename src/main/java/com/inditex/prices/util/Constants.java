package com.inditex.prices.util;

public class Constants {

    public static final String tittleAPI = "Inditex API";
    public static final String priceAPIGroup = "Prices API Group";

    public static final String version = "/1.0";
    public static final String pricesPath = "/prices";
    public static final String pricesValue = "Prices Service";

    public static final String getPricesValue = "Get price related specific product";
}
